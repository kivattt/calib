# calib

calib - Cellular automata library

gui.cpp is an optional gui for the library.\
tui.cpp is an optional tui for the library. Originally created so I could run calib on my android phone.

Want to change the width/height of the grid?
Call calibs set_size() function.

## TODO
* Make update\_using\_threads not crash
* Create a array <vector \<bool\>, 2> rulestring\_to\_rule function
* Create a Object rl\_to\_object function